---
title: Zoom
date: "2020-03-22"
description: "Mi az a Zoom?"
category: Tudásbázis
tags:
  - zoom
  - chat
  - videochat
  - post
---

A Zoom egy video konferencia alkalmazás.

## Telepítése

Töltsd le a Zoom telepítőt: https://zoom.us/download

Telepítsd fel, és be is fejezted. Nem kell regisztrálni!

Majd az előadó küldi a Zoom-linket, mellyel csatlakozni tudsz
a munkába. A linket küldheti email-ban, de akár Skype-on is.

<div class="alert alert-primary" role="alert">
<h4 class="alert-heading">Tippek</h4>
<ul>
<li>A Zoom első indításkor kérdezni fogja a neved, használd a saját neved!</li>
</ul>
</div>
