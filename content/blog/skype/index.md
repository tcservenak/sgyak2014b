---
title: Skype
date: "2020-03-22"
description: "Mi az a Skype?"
category: Tudásbázis
tags:
  - skype
  - chat
  - videochat
  - post
---

A Skype egy chat és hangalapú (melyet videó is kísérhet) kommunikációs alkalmazás.

## Telepítése

Töltsd le a Skype telepítőt: https://www.skype.com/en/get-skype/

Telepítsd fel, és regisztrálj, ha még nincs fiókod.

<div class="alert alert-primary" role="alert">
<h4 class="alert-heading">Tippek</h4>
<ul>
<li>Új fiók készítésekor használd a saját neved!</li>
<li>Jó ha a fiók neve legyen ugyanaz (vagy hasonló) mint az email címed</li>
<li>Keresd a társadtól, hogy vegyenek fel az osztály Skype csoportjába, ha még nem vagy ott</li>
</ul>
</div>
