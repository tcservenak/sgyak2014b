---
title: Google Drive
date: "2020-03-22"
description: "Mi az a Google Drive?"
category: Tudásbázis
tags:
  - google
  - drive
  - post
---

A Google Drive egy tárhely szolgáltatás, mely segítségével fájlokat,
képeteket tudsz tárolni, és megosztani.

Minden [Google fiók](/google-fiok) mellé ingyenesen jár 15GB tárhely.

## A Google Drive megnyitása

Kezdjük az [alaphelyzetből](/alaphelyzet):

- Nyiss egy új böngésző fület (Kattints a +-ra meglévő fül mellett, esetleg Ctrl+T)
- írd be a következő címet: https://drive.google.com/

## Feltöltés a Google Drive-ba

<div class="alert alert-primary" role="alert">
<h4 class="alert-heading">Fontos</h4>
Tarts rendet a Google Drive-ban, mert gyorsan a fejedre nőhet!
Tarts minden tantárgy számára külön-külön könyvtárat, esetleg legyen új mappa 
leckénként.
</div>

Nyisd meg a Google drive-ot. Menyj bele a kívánt könyvtárba (pl. tantárgy).
"Húzd bele" a feltöltendő fájlt.

## Megosztás a Google Drive-ból

Jobb egérgombbal kattintsa megosztandó fájlra:

![Context menu](./ctx-menu.png)

Kattints a "Megostási link létrehozása" ikonra a jobb felső sarokban, majd a Rendben:

![Share Link](./share.png)

**A link a vágolapon van.** 

Most kezdj el írni egy emailt, és a levél törzsébe 
vagy Skype üzenetbe illeszd bele a linket (Ctrl+V vagy Cmd+V).

