---
title: Diákoknak
date: "2020-03-23"
description: "Diákoknak"
category: Diákoknak
tags:
  - students
  - post
---

Tedd a következő dolgokat:

- Kövesd, időröl időre csekkold le saját [email fiókod](/email-fiok) ha a
telefonod nem értesít új levélről. 
Ide fognak érkezni a tanárok által küldött levelek, jellemzően feladattal.
<div class="alert alert-danger" role="alert">
<h4 class="alert-heading">Nagyon fontos</h4>
Ne hagyd email-jaidat olvasatlanul, vagy rosszabb, elolvasva de válasz nélkül! 
Ezzel valójában  a házi feladataid torlódnak fel, nem lesz időd megcsinálni őket!
Rosszabb esetben teljesen elveszíted a fonalat. Okosan tervezz!
</div>
<div class="alert alert-danger" role="alert">
<h4 class="alert-heading">Fontos</h4>
Az email <strong>nem chat</strong>. Nem kell azonnal elolvasni (a tanár küldheti 
azt este v éjszaka is, ahogy ideje kiadja!). Nem kell azonnal "nekilátni", inkább 
tervezz és oszd be az idődet.
</div>
<div class="alert alert-success" role="alert">
<h4 class="alert-heading">Jófejség lvl infinite</h4>
Ha az email az osztállyal kapcsolatos (akár csak osztályon belüli csoport
számára, pl idegen nyelvek) de a cimzett <strong>te voltál</strong> és nem a lista, segíts
az osztálytársaidon, és továbbítsd azt a listára.
</div>

- "Munkaidőben" kövesd a Skype csoportot. Itt tudsz segítséget kérni a társaidtól, vagy
kérdezni az OFÖ-től. De írhat is fontos dolgot az OFÖ. Egyúttal itt tudsz 
segíteni osztálytársaidnak is.
<div class="alert alert-danger" role="alert">
<h4 class="alert-heading">Fontos</h4>
Ne "lógj" állandóan a Skype-on (sem Viber-en). Iktass be néha pihenőt is.
</div>

- A meglévő Viber csoportot használd célszerúen. Ha sulival kapcsolatos kérdésed van
inkább a Skype-ot használd, mert ott jelen van az OFÖ is.

## Ha leveled érkezik

Figyelmesen olvasd el a levelet. Ha feladat, és "munkaidő" van, neki is láthatsz!
Ha nem, tervezd be mikor fogod megcsinálni.

## Ha a tanár választ vár

Ha beadandót kell visszaküldened, akkor azt illik a [Google Drive](/google-drive)-ba
feltölteni, és Linkkel megosztani.

Nézz utána, hova kell küldeni a választ! Ha a tanár nem úgy kapja meg a feladatot,
ahogy kérte, tekintheti a munkádat elégtelennek.

Ha a tanár a listára kér választ, a tanár eredeti levelére írj válaszlevelet.
Ha a tanár saját címére kéri a választ, új levelet írjál a megadott címre.

<div class="alert alert-primary" role="alert">
<h4 class="alert-heading">Tipp</h4>
Ha a tanár címére kell iskolai feladattal kapcsolatos emailt írnod, pl.
hogy elküld a feladat eredményét, akkor az email tárgyát kezd az osztály
nevével, pl. "6B Matek házi...". Gondolj bele, a tanár több osztályban is tanít(hat)
és napi 100 emailt is kaphat! Ezzel a pici gesztussal könnyítesz a munkáján!
</div>

## Tanácsok

- Az online iskola is iskola. Ugyanúgy kell részt venni rajta, ahogyan a rendes oktatásban. Pontosan kell 
érkezni az online órákra, asztalnál ülni, felöltözve.

- Összpontosíts! Otthon ülve sokkal több olyan dolog van, ami könnyedén elvonja az ember figyelmét. Csak 
rápillantani a telefonra, kiugrani egy pillanatra a konyhába, ránézni a háttérben futó játékra - fontos, hogy 
ezeket a dolgokat minimalizáljuk, kapcsoljuk ki a telefont, vagy rakjuk egy másik szobába. Úgy üljünk a 
gép előtt, mintha az iskolában lennénk.

- Légy felkészült! Az online tanulás különleges technikai feltételeket igényel és fontos, hogy ne azzal 
menjen el az óra, hogy azt biztosítani kell. Legyen már az óra előtt bekapcsolva a gép, megnyitva a 
szükséges program, ellenőrizve az internetkapcsolat és a mikrofon, hangfal. Így is bőven lesznek fennakadások, 
de talán egy kicsit kevesebb.

- Kerüljük el a hangzavart! A videókonferenciák során rettentően zavaró tud lenni, ha 10-20 ember hátérzaját 
kell hallgatni. Vannak olyan zajok, amikre nem is gondolunk, de a mikrofonon keresztül borzasztóan zavarók 
lehetnek: vakarózás, csámcsogás, kopogás az asztalon, csokipapír zörgetése. Éppen ezért tanácsos, hogy mindenki 
csak akkor kapcsolja be a mikrofonját, amikor éppen beszél.

- Egyszerre egy beszéljen. Nagyon más sok embernek együtt beszélgetni, ha csak a hangot halljuk. 
A videókonferenciáknál, online óráknál (főleg, hogy a hang és a kép gyakran el is van csúszva kicsit) fontos, 
hogy aki beszélni akar, jelentkezzen, például a videókonferencia csetjében. Érdemes megállapodni ennek a pontos 
protokolljában. Biztosan lesz nagyon sok dolog, ami nem fog működni, vagy itt-ott fennakadások lesznek benne. 
Megszakad a net, rossz a link, elmegy a hang. Csak akkor fog működni a dolog, ha túl tudunk lendülni mindegyiken.

- Csak akkor fog működni ez az egész, ha mindnyájan akarjuk. A mostani helyzet extra konstruktivitást és különleges 
rugalmasságot követel mindenkitől. Ez az a pillanat, amikor a leginkább van szükségünk a kölcsönös bizalomra 
és arra, hogy közös dologként kezeljük a tanulást.

<div class="alert alert-primary" role="alert">
<h4 class="alert-heading">Tipp</h4>
Tanárok és diákok számára nagyon jó összefoglaló cikk 
<a href="https://index.hu/techtud/2020/03/22/digitalis_atallas_oktatas_iskola_tanacsok_javaslatok/">olvasható az Indexen</a>.
</div>
