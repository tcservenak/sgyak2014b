---
title: "Levelezői Lista Etikett (netikett)"
date: "2020-03-22"
description: "Netes illemtan"
category: Tudásbázis
tags:
  - netikett
  - post
---

Az emberi kommunikáció eleve nagyon komplex, és a dolgok csak rosszabbodnak
ilyen "száraz" médiumon mint az email vagy a chat (nincs kontextus, body
language, kacsintás, csak a száraz leírt tartalom).

Fontos megkülönböztetni az

- **Új levél írása**: ez "témaindító", és sok levelező összefésüli a 
témaindító levélre adott válaszokat, ezzel is könnyíteni próbál az emberen.

- **Meglévő levélre válasz**: ez "válasz" a témaindítóra, úgymond 
"besorolódik a téma indító levél alá". Ilyen levelekben választ írjunk, 
**ne indítsunk új témákat**.

Ennélfogva jó lenne, ha **mindenki betartaná** a következő szabályokat:


## Zaj
   
  - Pista (új levél): Szereztem laptopot!
    - Jóska (válasz): Zsíír! (== nem jó)
    - Karcsi (válasz) : Nagyon jóóó! (== nem jó)
    - Laci (válasz): Telepítési segítség kell?  (== jó)

### Miért rossz?

Zajt kelt, nem közöl érdemi információt, az eredeti üzenet elveszik 
a zajtengerben. Az gratulációt személyesen, vagy "direkt" módon közöld,
ne a listán (természetesen lehet poénkodni, de azt is ésszel tegyük, 
mert ha rossz mail folyamban tesszük azt, az pl. fontos információ 
szem előtt tévesztéséhez vezethet). Igyekezz érdemben hozzászólni.

## Tárgy elvesztése

  - Pista (új levél): Van-e valakinek XY töltője kölcsön?
    - Jóska (válasz): Nincs (== nem jó)
    - Karcsi (válasz): Van! De mikor lesz szülői értekezlet? (== nem jó)
    - Laci (válasz): Holnap. (== nem idevágó)

### Miért rossz?

"Eltériti" a témát. Az eredeti kérdésre CSAKIS érdemi és odaillő választ írjunk.
Ebben az esetben a "van" lenne az (gondolj bele, 30 ember írja hogy "nincs").
De Karcsi még rosszabbat tett: "hijack"/elrabolta a témát.
Végül, Laci válasza a "Van-e valakinek XY töltője kölcsön?" kérdésre hogy
"Holnap" totál nonszensz. 

### Ilyenkor a helyes

  - Pista (új levél): Van-e valakiken XY töltője kölcsön?
    - Karcsi (válasz): Van! Írjál nekem.


  - Karcsi (új levél): Mikor lesz szülői értekezlet?
    - Laci (válasz): Holnap.
