---
title: Email fiók
date: "2020-03-22"
description: "Mi az email fiók?"
category: Tudásbázis
tags:
  - email
  - post
---

<div class="alert alert-danger" role="alert">
<h4 class="alert-heading">Fontos</h4>
Ha osztozol a gépen valakivel (tesó, apa), mindig győződj
meg róla, hogy a saját <a href="/google-fiok">Google Fiókod</a> használod!
</div>

Az email címet felfoghatod hasonlóan mint a lakáscímed. Ez egy "azonosító"
ami alapján bárki a világon tud neked emailt írni.

Viszont, az emberek költöznek, és lakáscímük tud változni, de az
email cím ritkán változik, ha megfogadod a jó tanácsokat.

A XXI. században szinte elvárt, hogy mindenkinet legyen email címe.

És még egy fontos dolog: ez email az nem chat!

## Hogy néz ki egy email

Az email rendelkezik küldővel, címzettel (ezek email címek), tárggyal (egysoros
"összefoglaló") és magával a tartalommal. Esetleg lehet neki csatolt állománya.

Az email cím két részből áll, melyet egy `@` (angolul "at") betű köt ossze, például:

`sgyak2014b-osztaly@googlegroups.com`

Az első rész a mail fiók neve, a második rész a szolgáltató (v "domain") neve.

Ha te küldesz emailt, te leszel a küldő. Ha kaptál emailt, te vagy a címzett (közvetlenül
vagy közvetve). Levelezői lista esetében, a küldő a listát jelöli meg címzettnek,
de a lista minden tagja megkapja a levelet.

A levél eredetét könnyen leellenőrizheted a küldő alapján.

![mail properties](./mail-props.png)

### Tippek

- Ha a kapott levél címzettje nem te vagy, hanem a levelezői lista, akkor biztos
lehetsz abban, hogy minden társad megkapta ezt.
- Ha a kapott levél címzettje te vagy, és a levél a teljes osztályt érinti, továbbítsd
az az osztály listára.

## Új email írása

Ha egy új "témát" akarsz indítani, akkor új emailt írsz. Kezdjük az 
[alaphelyzetből](/alaphelyzet): balra fenn van egy "plusz" jel, ezzel kezdesz
új emailt.

![compose](./compose.png)

## Kapott emailra válasz

Ha emailt kaptál, és válszolni akarsz rá, akkor a "Válasz" opciót kell kiválasztanaod
ez email olvasása közben.

![reply](./reply.png)

Fontos: válasz írasakor, maradj az email témájánál, ez **nem chat**. Ha a szerzőtöl
valami független témáról kérdeznéd, írj új levelet.

## Kapott email továbbítása

Hasonlóan a válasz esetén, ha a válasz ikon melletti pont-pont-pont lebomló menüre
kattintasz, ki tudod választani a Továbbítást. Így a levelet szőröstül-bőröstül
továbbítani tudod.

<div class="alert alert-primary" role="alert">
<h4 class="alert-heading">Tippek</h4>
<ul>
<li>Ne legyen gyerekes az emailcímed, jobb ha hasonlít a tényleges nevedre. Az 
ami ma "menő", jövőre már ciki lehet!</li>
<li>Igyekezz egy email címet használni és megtartani. Gondolj bele, ha 10 év alatt 10 fiókot
"elfogyasztasz", hogy fognak az emberek rád találni?</li>
</ul>
</div>
