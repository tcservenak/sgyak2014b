---
title: Tanároknak
date: "2020-03-23"
description: "Tanároknak"
category: Tanároknak
tags:
  - teachers
  - post
---

Ha Ön tanár, itt egy kis segítséget találhat, külön csoportosítva kommunikációs módonként.

## Írásban

Ha írásban szeretne az osztályhoz fordulni, küldjön emailt az
<a href="mailto:sgyak2014b-osztaly@googlegroups.com">sgyak2014b-osztaly@googlegroups.com</a>
osztály lista email címére. Így biztos lehet benne, hogy minden 
diák megkapja az üzenetet.

Ha feladatot oszt ki, célszerű azt pontosan leírni mi a tennivaló, 
mit vár el, van-e határidő, a szokásost.

Ha választ vár vissza a gyerekektől (pl. feladat megoldását), írja meg a feladat
keretében mi a határidő és milyen módon kéri vissza a választ. Továbbá, legyen
szabatos abban, hova kéri vissza a válaszokat:

- Ha a diákok az emailra (a "feladat emailra") küldenek válaszokat, 
akkor minden diák látni fogja egymás válaszát!

- Ha fontos, hogy a diákok ne lássák egymás válaszait, a feladat leírásában 
adja meg saját email címét, mint a válasz beküldési címét, és minden diák 
Új emailt fog írni Önnek! Egyúttal, nem látják egymás válaszát.

## Szóban (egyirányú)

A YouTube Live segítségével tud egyirányú vizuális előadást tartani.
A visszacsatolás a stream kommentjeiben történhet.

## Szóban (kétirányú)

Kétirányú frontális munkára a <a href="/skype">Skype</a> vagy 
<a href="/zoom">Zoom</a> az ajánlott.

<div class="alert alert-primary" role="alert">
<h4 class="alert-heading">Tipp</h4>
Tanárok és diákok számára nagyon jó összefoglaló cikk 
<a href="https://index.hu/techtud/2020/03/22/digitalis_atallas_oktatas_iskola_tanacsok_javaslatok/">olvasható az Indexen</a>.
</div>
