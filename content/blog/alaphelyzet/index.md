---
title: Alaphelyzet
date: "2020-03-22"
description: "A kiindulópont minden egyes utasításhoz"
category: Tudásbázis
tags:
  - start
  - post
---

Mindig igyekezz az "alaphelyzetől" indulni. Ez legyen hasonló
dolog. mint a telefonon, tableten a "Home" gomb megnyomása,
így a telefon Home képernyőjére kerülsz, bármilyen alkalmazásban
is voltál.

Ezzel magadon segítesz,
mert könyebb lesz követni az utasításokat, és idővel jobban
megismered a lépések közötti összefüggéseket is.

A mi esetünkben, az "alaphelyzet" legyen a következő:

![alaphelyzet](./alaphelyzet.png)

## Ami fontos:
- egy böngésző ablak egy füle legyen csak nyitva
- a fülben a https://mail.google.com legyen megnyitva
- győződj meg róla (jobb felső sarok), hogy a **saját fiókodban vagy**, 
nem a tesóéban, apuéban.

Innen már síma út vezet a további munkához.
