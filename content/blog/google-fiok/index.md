---
title: Google fiók
date: "2020-03-22"
description: "Mi az a Google fiók?"
category: Tudásbázis
tags:
  - google
  - email
  - post
---

A Google Fiók egyetlen e-mail címből és jelszóból álló központi 
bejelentkezési fiók.

<div class="alert alert-primary" role="alert">
<h4 class="alert-heading">Fontos</h4>
A jelszavad soha senkivel sem oszd meg. Tekints a jelszóra
hasonlóan, mint a lakásod kulcsára: szülőnek még-még odaadod, de
idegennek semmiképp sem.
</div>


A Google fiók segítségével több szolgáltatást is el tudsz érni, mint 
például:

 * GMail - email/levelező szolgáltatás
 * YouTube - video szolgáltatás
 * Google Groups - levelezői listák
 * stb

## Több fiók esetén

Ha olyan számítógépen dolgozol, amit többen közösen használtok, több Google
Fiók is be lehet állítva. Mindig győződj meg, hogy a saját Google Fiókod
használod:

Nyisd meg a https://www.google.com/ oldalt.

A jobb felső sarokban, kattints a kerek képecskére (vagy iniciálére, ha nincs
profilképed):

![change account](./change-account.png)

A lebomló menüben több fiók is lehet! Válaszd ki a saját fiókodat,
írd be a saját jelszavad (ha kéri), is innen folytasd az [alaphelyzetbe](/alaphelyzet).
